# Development Setup
## Installation
Project is developed using NodeJS v14. However any NodeJS version > LTS of v10 should work fine.

In the root directory of project, simply run `npm ci` to install from `package-lock.json` or `npm install` to install from both `package.json` and `package-lock.json`.

## Running Application in Development Environment
Application is using `nodemon` and reloads on code changes. Simply use `npm start` to start `nodemon`. After starting, navigate to `http://localhost:8080` in browser to see if application is started correctly.

## Bundling Application in Docker Container
`Dockerfile` and `docker-compose.yml` files are also provided in source code.

### Build Container
Run following command to build the container image.

```docker build . -t swvl-notif-api```

### Start and Stop the Container
Run following command to start the container.

```docker-compose up```

or to run it in background, add the option `-d` like this.

```docker-compose up -d```

## Testing Application
Application is currently not connecting to any external service like push notifiation, or an SMS provider. It logs output on console for each type of notification. To test application, details of API calls are exported from Postman, and are included in source code. You can simply import `SWVL_Challenge.postman_collection.json` file in Postman and fire the calls after running application either in terminal, or as a docker container.

# Details about Code and API Calls

## Used Frameworks/Libraries
1. API is built using `ExpressJS`.
2. `Sqlite3` is used as database engine.
3. Also used `Sequelize` as ORM to handle database schema and CRUD operations.

## Important Code Files
### 1. `db-config/config.js`
This file contains database artifacts like entities, configuration of ORM, and database hooks. Specifically:
1. Database entities like: `Customer`, `Booking`, `Vehicle`, `Provider`, `Notification`.
2. Relationships between entities.
3. Two database hooks: one to be triggered on creation of booking, while other to be invoked on change in status of booking.

### 2. `app.js`
This file contains all of the API endpoints and also starts a scheduler that runs every 10 seconds to process pending notifications. Following API calls are provided:
1. GET `/seed_data` drops all tables, and create new tables with data defined in `db-config/config.js`.
2. GET `/customer/:id` can fetch customer based on Id.
3. GET `/booking/:status` fetches list of bookings based on status (`new`, `assigned`, `completed`, `cancelled`).
4. POST `/booking` creates a new booking. On creation, database hook creates new notification in database which is then processed by pending notification handler.
5. PATCH `/booking/:id/:status` can be used to change status of booking. Change in status triggers hook that creates new notification.
6. POST `/notification/:type` custom personalized and group notifications can also be created in database using this call.

### 3. `notifications/notification-handler.js`
This file contains methods for handling pending notification. Notifications are created in database with status. Pending notification handler sends notifications (prints on console for now). This method also checks if per minute limit is reached. If limit is reached, it doesn't send notification in current invocation, and tries to send again in next invocation (after 10 seconds).

# Skipped Requirements
Please note, due to time constraints, I did not implement following features:
1. Unit testing.
2. Linking the API with Push Notification service (I would have implemeted it with Firebase).
3. I also wanted to create a web frontend where sent notifications could be tested.
4. Added Swagger documentation but it was cumbersome right now to create `json` file, hence documented API calls here in README, and provided Postman calls for ease of use.
