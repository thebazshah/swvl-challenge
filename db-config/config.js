const { Sequelize, Model, DataTypes } = require("sequelize")
const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "app.db"
})

// Database entities

class Customer extends Model {}
Customer.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  full_name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  mobile: {
    type: DataTypes.STRING,
    allowNull: false
  },
  push_id: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize,
  modelName: "customer",
  tableName: "customers"
})

class Booking extends Model {}
Booking.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  booking_time: {
    type: DataTypes.DATE,
    allowNull: false
  },
  status: {
    type: DataTypes.ENUM,
    values: ["new", "assigned", "in-progress", "completed", "cancelled"]
  }
}, {
  sequelize,
  modelName: "booking",
  tableName: "bookings"
})

class Vehicle extends Model {}
Vehicle.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  make: {
    type: DataTypes.STRING,
    allowNull: false
  },
  model: {
    type: DataTypes.STRING,
    allowNull: false
  },
  reg_number: {
    type: DataTypes.STRING,
    allowNull: false
  },
  driver_name: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize,
  modelName: "vehicle",
  tableName: "vehicles"
})

class Provider extends Model {}
Provider.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  type: {
    type: DataTypes.ENUM,
    values: ["sms", "push-notif"]
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false
  },
  limit_per_minute: {
    type: DataTypes.INTEGER,
    allowNull: true
  }
}, {
  sequelize,
  modelName: "provider",
  tableName: "providers"
})

class Notification extends Model {}
Notification.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  title: {
    type: DataTypes.STRING,
    allowNull: true
  },
  text: {
    type: DataTypes.STRING,
    allowNull: false
  },
  type: {
    type: DataTypes.ENUM,
    values: ["personalized", "group"]
  },
  comm_type: {
    type: DataTypes.ENUM,
    values: ["sms", "push-notif"]
  },
  status: {
    type: DataTypes.ENUM,
    values: ["new", "sent", "failed"]
  }
}, {
  sequelize,
  modelName: "notification",
  tableName: "notifications"
})

// Relationships and cardinality between entities

Customer.hasMany(Booking)
Customer.hasMany(Notification)
Vehicle.hasMany(Booking)
Provider.hasMany(Notification)

// create tables if doesn't exist
Customer.sync()
Booking.sync()
Vehicle.sync()
Provider.sync()
Notification.sync()

// Seed database with some data.
const seedData = () => {
  sequelize
  .sync({ force: true })
  .then(() => {
    const c1 = Customer.create({ full_name: "Muhammad Shahbaz", email: "thebazshah@gmail.com", mobile: "+923055111151", push_id: "" })
    const c2 = Customer.create({ full_name: "Muhammad Shahbaz", email: "thebazshah@hotmail.com", mobile: "+923225244425", push_id: "" })
    const v1 = Vehicle.create({ make: "Suzuki", model: "Bolan", reg_number: "RIP-3256", driver_name: "Ahmed Ali" })
    const v2 = Vehicle.create({ make: "Toyota", model: "Hiace", reg_number: "LHR-1234", driver_name: "Farooq Rasheed" })
    const p1 = Provider.create({ type: "sms", title: "SMS", limit_per_minute: 30 })
    const p2 = Provider.create({ type: "push-notif", title: "Push Notifications", limit_per_minute: 100 })
  });
}

// Database hook in case of new booking
Booking.afterCreate(async (b, o) => {
  const booking = b.dataValues
  const customer = await Customer.findByPk(booking.customerId)
  const vehicle = await Vehicle.findByPk(booking.vehicleId)
  const provider = await Provider.findOne({ where: { type: "sms" }})
  const title = "Booking Created"
  const text = `Dear ${customer.full_name},
    Your booking has been created and assigned to ${vehicle.driver_name}.
    Please wait for ${vehicle.make} ${vehicle.model} (Reg: ${vehicle.reg_number}) at specified location.`
  const notification = Notification.create({
    title,
    text,
    type: "personalized",
    comm_type: "push-notif",
    status: "new",
    customerId: customer.id,
    providerId: provider.id
  })
})

// Database hook in case of booking status change
Booking.afterUpdate(async (b, o) => {
  const booking = b.dataValues
  const customer = await Customer.findByPk(booking.customerId)
  const vehicle = await Vehicle.findByPk(booking.vehicleId)
  const provider = await Provider.findOne({ where: { type: "push-notif" }})
  const title = "Booking Status Updated"
  let shouldSend = true
  let text
  switch(booking.status) {
    case "assigned":
      text = `Dear ${customer.full_name},
        Your booking has been assigned to ${vehicle.driver_name}.
        Please wait for ${vehicle.make} ${vehicle.model} (Reg: ${vehicle.reg_number}) at specified location.`
      break
    case "in-progress":
      text = `Dear ${customer.full_name},
        Thank you for finding the driver. Your ride is in progress. Have safe journey.`
      break
    case "completed":
      text = `Dear ${customer.full_name},
        Your booking with ${vehicle.driver_name} is complete. Your bill for this ride is Rs. 250.
        Don't forget to rate the driver.`
      break
    case "cancelled":
      text = `Dear ${customer.full_name},
        We are sorry to inform that your booking has been cancelled with ${vehicle.driver_name}.`
      break
    default:
      shouldSend = false
  }
  if (shouldSend) {
    const notification = await Notification.create({
      title,
      text,
      type: "personalized",
      comm_type: "push-notif",
      status: "new",
      customerId: customer.id,
      providerId: provider.id
    })
  }
})

module.exports = {
  sequelize: sequelize,
  Customer: Customer,
  Booking: Booking,
  Vehicle: Vehicle,
  Provider: Provider,
  Notification: Notification,
  seedData: seedData
}
