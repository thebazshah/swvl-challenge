const express = require('express')
const cors = require('cors')
const swaggerUi = require('swagger-ui-express');

const { Booking, Customer, seedData, Notification, Provider } = require("./db-config/config")
const swaggerDocument = require('./swagger.json');
const { processPendingNotifications } = require("./notifications/notification-handler")

const port = 8080
const app = express()

const corsOptions = {
  origin: 'http://localhost:80',
  optionsSuccessStatus: 200
}

app.use(cors(corsOptions))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/', (req, res, next) => {
  res.json({ message: "SWVL Notification API is running." })
})

app.get('/seed_data', (req, res, next) => {
  seedData()
  res.json({ message: "Seed data function executed." })
})

app.get('/customer/:id', (req, res, next) => {
  Customer.findByPk(req.params.id)
    .then(customer => {
      if (customer === null || customer === undefined) {
        res.status(404).send({
          message: "Customer didn't exist against this primary key."
        })
      }
      res.json(customer)
    })
    .catch(err => {
      throw err
    })
})

app.get("/booking/:status", (req, res, next) => {
  const status = req.params.status;
  Booking.findAll({
    where: {
      status
    }
  })
  .then(bookings => {
    if (bookings === null
    || bookings === undefined
    || (Array.isArray(bookings) && bookings.length === 0)) {
      res.status(404).send({
        message: "Bookings according to criteria do not exist."
      })
    } else {
      res.json(bookings)
    }
  })
})

app.post("/booking", (req, res, next) => {
  const newBooking = req.body
  newBooking.status = "new"
  Booking.create(newBooking).then(b => {
    if (b === null || b === undefined) {
      res.status(200).send({ message: "Booking could not be created." })
    } else if (b.id > 0) {
      res.json(b)
    }
  }).catch(e => res.status(500).send({ message: "An error occurred."}))
})

app.patch("/booking/:id/:status", (req, res, next) => {
  const id = req.params.id
  const status = req.params.status

  Booking.findByPk(id).then(async b => {
    if (b === null || b === undefined) {
      res.status(200).send({ message: "Booking could not be updated." })
    } else {
      const booking = await b.update({ status })
      if (booking === null || booking === undefined) {
        res.status(200).send({ message: "Booking could not be updated." })
      } else {
        res.json(booking)
      }
    }
  }).catch(e => res.status(500).send({ message: "An error occurred. Message: " + e}))
})

app.post("/notification/:type", async (req, res, next) => {
  const type = req.params.type;
  const newNotification = req.body
  const provider = await Provider.findOne({ where: { type: newNotification.comm_type }})
  if (type === "personalized") {
    const { customerId } = newNotification || {};
    if (customerId === null || customerId === undefined) {
      res.status(422).send({ message: "Recipient (user) is not mentioned for notification"})
    } else {
      const notification = await Notification.create({
        title: newNotification.title,
        text: newNotification.text,
        type: "personalized",
        comm_type: newNotification.comm_type,// "push-notif",
        status: "new",
        customerId: customerId,
        providerId: provider.id
      })
      if (notification === null || notification === undefined) {
        res.status(200).send({ message: "Notification could not be created." })
      } else {
        res.json(notification)
      }
    }
  } else if (type === "group") {
    const { customers } = newNotification || {}
    if (customers === null || customers === undefined) {
      res.status(422).send({ message: "Recipients (customers) are not mentioned for notification"})
    } else if (!Array.isArray(customers)) {
      res.status(422).send({ message: "List of recipients is not provided."})
    } else {
      let notifications = []
      customers.map(c => {
        notifications.push({
          title: newNotification.title,
          text: newNotification.text,
          type: "group",
          comm_type: newNotification.comm_type,
          status: "new",
          customerId: c,
          providerId: provider.id
        })
      })
      notifications = await Notification.bulkCreate(notifications)
      if (notifications === null || notifications === undefined || !Array.isArray(notifications)) {
        res.status(200).send({ message: "Could not send specified notifications"})
      } else {
        res.json(notifications)
      }
    }
  } else {
    res.status(422).send({ message: "Wrong type specified. Either use personalized or group." })
  }
})

processPendingNotifications()

app.listen(port, function () {
  console.log(`Swvl Notification app listening on http://localhost:${port}`)
})
