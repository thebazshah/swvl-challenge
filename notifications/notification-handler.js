const { Op } = require("sequelize")
const { Customer, Notification, Provider } = require("../db-config/config")

const processPendingNotifications = () => {
  setInterval(async () => {
    console.log("################################################################################")
    let sentSms = 0
    let sentPush = 0
    // get providers from db
    const smsProvider = await Provider.findOne({ where: { type: "sms" }})
    const pushProvider = await Provider.findOne({ where: { type: "push-notif" }})
    // get pending notifications from db
    const pending = await Notification.findAll({ where: { status: "new" }})
    // see how many sms and push notifications has been sent within min
    const minAgo = new Date(Date.now() - 60000)
    const withinMinSms = await Notification.count({
      where: {
        comm_type: "sms",
        status: "sent",
        updatedAt: {
          [Op.gt]: minAgo
        }
      }
    })
    const withinMinPushNotif = await Notification.count({
      where: {
        comm_type: "push-notif",
        status: "sent",
        updatedAt: {
          [Op.gt]: minAgo
        }
      }
    })
    console.log("withinMinSms", withinMinSms)
    console.log("withinMinPushNotif", withinMinPushNotif)
    // send messages
    for (const notification of pending) {
      const customer = await Customer.findByPk(notification.customerId)
      if (customer !== null && customer !== undefined) {
        if (notification.comm_type === "sms" && (withinMinSms + sentSms) < smsProvider.limit_per_minute) {
          notification.mobile = customer.mobile
          sendSmsNotification(notification)
          sentSms++
        } else if (notification.comm_type === "push-notif"
          && (withinMinPushNotif + sentPush) < pushProvider.limit_per_minute) {
          notification.push_id = customer.push_id
          sendPushNotification(notification)
          sentPush++
        }
      }
    }
    console.log("################################################################################")
  }, 10000)
}

const sendPushNotification = async notification => {
  console.log(notification.text)
  // update status in database
  delete notification.push_id
  notification.update({ status: "sent" })
}

const sendSmsNotification = async notification => {
  console.log(notification.mobile, notification.text)
  // update status in database
  delete notification.mobile
  notification.update({ status: "sent" })
}

module.exports = {
  processPendingNotifications: processPendingNotifications
}
